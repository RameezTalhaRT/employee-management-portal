package com.example.employee_management_system.service_Department;

import com.example.employee_management_system.Repository_Department.Department_Repository;
import com.example.employee_management_system.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Department_service {
    @Autowired

    private Department_Repository departmentRepository;
    public void add_department(Department department){
        departmentRepository.save(department);
    }
    public void delete_department(Long id){
        departmentRepository.deleteById(id);
    }
    public Optional<Department> get_department_by_id(Long id){
        return Optional.ofNullable(departmentRepository.findById(id).orElse(null));
    }
    public List<Department> get_all_department(){
        return departmentRepository.findAll();
    }

    public void update_department(Long id, Department updatedep){
        if (departmentRepository.existsById(id)){
            updatedep.setId(id);
            departmentRepository.save(updatedep);
        }
    }
}
