package com.example.employee_management_system.services;

import com.example.employee_management_system.Repository.Employee_Repository;
import com.example.employee_management_system.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Employee_service {
    @Autowired

    private Employee_Repository emprepository;
    public void add_employee(Employee employee){
        emprepository.save(employee);
    }
    public void delete_employee(Long id){
        emprepository.deleteById(id);
    }
    public Optional<Employee> get_employee_by_id(Long id){
        return Optional.ofNullable(emprepository.findById(id).orElse(null));
    }
    public List<Employee> get_all_emp(){
        return emprepository.findAll();
    }

    public void update_employee(Long id, Employee updateemp){
        if (emprepository.existsById(id)){
            updateemp.setId(id);
            emprepository.save(updateemp);
        }
    }
}
