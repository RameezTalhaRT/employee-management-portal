package com.example.employee_management_system.service_experience;

import com.example.employee_management_system.Repository_Experience.Emp_Background_Exp_Repository;
import com.example.employee_management_system.entity.Emp_Background_Exp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Emp_Background_Exp_service {
    @Autowired

    private Emp_Background_Exp_Repository backgroundRepository;

    public void add_employee_experience(Emp_Background_Exp experience) {
        backgroundRepository.save(experience);
    }

    public void delete_employee_experience(Long id) {
        backgroundRepository.deleteById(id);
    }

    public Optional<Emp_Background_Exp> get_employee_experience_by_id(Long id) {
        return Optional.ofNullable(backgroundRepository.findById(id).orElse(null));
    }

    public List<Emp_Background_Exp> get_all_employee_experience() {
            return backgroundRepository.findAll();

    }
    public void  update_experience(Long id, Emp_Background_Exp updateexp) {
        if (backgroundRepository.existsById(id)) {
            updateexp.setId(id);
            backgroundRepository.save(updateexp);
        }
    }
}