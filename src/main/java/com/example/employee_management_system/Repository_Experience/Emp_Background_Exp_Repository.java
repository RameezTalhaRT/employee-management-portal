package com.example.employee_management_system.Repository_Experience;

import com.example.employee_management_system.entity.Emp_Background_Exp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


public interface Emp_Background_Exp_Repository extends JpaRepository<Emp_Background_Exp,Long> {
}
