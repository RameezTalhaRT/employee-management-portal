package com.example.employee_management_system.Emp_Controller;

import com.example.employee_management_system.user.User;
import com.example.employee_management_system.user_repo.User_Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping
public class Admin_controller {
    @Autowired
    private User_Repository userRepository;

    @GetMapping()
    public String loginform() throws InterruptedException {
        Thread.sleep(800);
        return "employees/Login";
    }
    @PostMapping("/login")
    public String login(@ModelAttribute("user") User user) throws InterruptedException {
        User admin = userRepository.findByUsername(user.getUsername());
        Thread.sleep(800);
        if (admin == null || !user.getPassword().equals(admin.getPassword())) {
            return "redirect:/";
        }
        return "redirect:/employees";
    }
    @GetMapping("/registerform")
    public String showregisterform(Model model) throws InterruptedException {
        model.addAttribute("user", new User());
        Thread.sleep(800);
        return "employees/Register";
    }
    @PostMapping("/register")
    public String register(@ModelAttribute("user") User user) throws InterruptedException {
        //Thread.sleep(800);
        userRepository.save(user);
        return "redirect:/employees";
    }
    @GetMapping("/About")
    public String About() throws InterruptedException {
        Thread.sleep(800);
        return "employees/About";
    }

    /*@PostMapping("/register")
    public String save_user(Admin admin ){
        admin_service.add_Admin(admin);
        return "redirect:/employees";
    }*/

}

