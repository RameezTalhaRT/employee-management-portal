package com.example.employee_management_system.Emp_Controller;

import com.example.employee_management_system.Skill_service;
import com.example.employee_management_system.entity.Emp_Background_Exp;
import com.example.employee_management_system.entity.Employee;
import com.example.employee_management_system.entity.Employee_Qualification;
import com.example.employee_management_system.entity.Skill;
import com.example.employee_management_system.service_Qualification.Qualification_service;
import com.example.employee_management_system.service_experience.Emp_Background_Exp_service;
import com.example.employee_management_system.services.Employee_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("/employees")
public class Employee_controller {
    @Autowired
    private Employee_service empService;
    private Emp_Background_Exp_service expService;
    private Qualification_service qualificationService;
    private Skill_service skillService;
    @GetMapping
    public String Homepage() throws InterruptedException {
        Thread.sleep(800);
        return "employees/Home";
    }

    //Employee
    @GetMapping("/employeelist")
    public String employee_list(Model model) throws InterruptedException {
        Thread.sleep(800);
        model.addAttribute("employees", empService.get_all_emp());
        return "employees/list_employee";
    }

    @GetMapping("/form")
    public String employee_form(Model model) {
        model.addAttribute("employee", new Employee());
        return "employees/employee_form";
    }

    @PostMapping("/save")
    public String save_form(Employee employee) {
        empService.add_employee(employee);
        return "redirect:/employees";
    }

    @PostMapping("/upload")

    public String upload_form(@RequestParam("empimage") MultipartFile file){
        if (file.isEmpty()) {
            return "redirect:/employees";
        }
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get("static/profile" + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/employees";
    }

@GetMapping("/updateform")
    public String update_form(@RequestParam("employeeid") Long id, Model model) {
        model.addAttribute("employee", empService.get_employee_by_id(id));
        return "employees/update_employee";
    }

    @DeleteMapping("/delete")
    public String delete_form(@RequestParam("employeeid") Long id) {
        empService.delete_employee(id);
        return "redirect:/employees";
    }

    //Experience
    @GetMapping("/experience_list")
    public String experience_list(Model model) throws InterruptedException {
        Thread.sleep(800);
        expService = new Emp_Background_Exp_service();
        model.addAttribute("experiences", expService.get_all_employee_experience());
        return "employees/list_experience";
    }
    @PostMapping("/save_exp")
    public String save_exp_form(Emp_Background_Exp exp) {
        expService = new Emp_Background_Exp_service();
        expService.add_employee_experience(exp);
        return "redirect:/employees";
    }

    //Qualification
    @GetMapping("/qualificationlist")
    public String qualification_list(Model model) throws InterruptedException {
        Thread.sleep(800);
        model.addAttribute("qualifications", qualificationService.get_all_qualification());
        return "employees/list_qualification";
    }
    @PostMapping("/save_qualification")
    public String save_qualification_form(Employee_Qualification qualification) {
        qualificationService.add_qualification(qualification);
        return "redirect:/employees";
    }

    //Skill
    @GetMapping("/skilllist")
    public String skill_list(Model model) throws InterruptedException {
        Thread.sleep(800);
        model.addAttribute("skills", skillService.get_all_skill());
        return "employees/list_skill";
    }
    @PostMapping("/save_skill")
    public String save_skill_form(Skill skill) {
        skillService.add_skill(skill);
        return "redirect:/employees";
    }

}
