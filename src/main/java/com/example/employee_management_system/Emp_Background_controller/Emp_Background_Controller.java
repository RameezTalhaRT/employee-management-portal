package com.example.employee_management_system.Emp_Background_controller;

import com.example.employee_management_system.entity.Department;
import com.example.employee_management_system.entity.Emp_Background_Exp;
import com.example.employee_management_system.entity.Employee;
import com.example.employee_management_system.entity.Employee_Qualification;
import com.example.employee_management_system.service_Department.Department_service;
import com.example.employee_management_system.service_Qualification.Qualification_service;
import com.example.employee_management_system.service_experience.Emp_Background_Exp_service;
import com.example.employee_management_system.services.Employee_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/emp")
public class Emp_Background_Controller {
    @Autowired
    private Employee_service empService;
    private Emp_Background_Exp_service backgroundExpService;
    private Department_service departmentService;
    private Qualification_service qualificationService;
    //private Admin_Service adminService;
    //Employee
    @GetMapping
    public List<Employee> getallemp() {
        return empService.get_all_emp();
    }

    @GetMapping("/find/{id}")
    public Optional<Employee> findemp(@PathVariable long id){
        return empService.get_employee_by_id(id);
    }
    @PostMapping("/add")
    public void Addemp(@RequestBody Employee employee) {
        empService.add_employee(employee);
    }

    @PutMapping("/update/{id}")
    public void updateemp(@PathVariable long id ,@RequestBody Employee updateemp) {
        empService.update_employee(id,updateemp);
    }
    @DeleteMapping("/delete/{id}")
    public void deleteemp(@PathVariable Long id){
        empService.delete_employee(id);
    }

//Department
    @GetMapping("/alldep")
    public List<Department> getalldep() {
        return departmentService.get_all_department();
    }

    @GetMapping("/finddep/{id}")
    public Optional<Department> finddep(@PathVariable long id){
        return departmentService.get_department_by_id(id);
    }
    @PostMapping("/adddep")
    public void Addemp(@RequestBody Department department) {
        departmentService.add_department(department);
    }

    @PutMapping("/updatedep/{id}")
    public void update_dep(@PathVariable long id ,@RequestBody Department updatedep) {
        departmentService.update_department(id,updatedep);
    }
    @DeleteMapping("/deletedep/{id}")
    public void deletedep(@PathVariable Long id){
        departmentService.delete_department(id);
    }

    //Experience
    @GetMapping("/getallexp")
    public List<Emp_Background_Exp> getallexp() {
        return backgroundExpService.get_all_employee_experience();
    }

    @GetMapping("/findexp/{id}")
    public Optional<Emp_Background_Exp> findexp(@PathVariable long id){
        return backgroundExpService.get_employee_experience_by_id(id);
    }
    @PostMapping("/addexp")
    public void Addexp(@RequestBody Emp_Background_Exp exp) {
        backgroundExpService.add_employee_experience(exp);
    }

    @PutMapping("/updateexp/{id}")
    public void update_exp(@PathVariable long id ,@RequestBody Emp_Background_Exp updateexp) {
        backgroundExpService.update_experience(id,updateexp);
    }
    @DeleteMapping("/deleteexp/{id}")
    public void deleteexp(@PathVariable Long id){
        backgroundExpService.delete_employee_experience(id);
    }

    //Qualification
    @GetMapping("/getallqualification")
    public List<Employee_Qualification> getallqualification() {
        return qualificationService.get_all_qualification();
    }

    @GetMapping("/findqualification/{id}")
    public Optional<Employee_Qualification> findqualification(@PathVariable long id){
        return qualificationService.get_qualification_by_id(id);
    }
    @PostMapping("/addqualification")
    public void Addqualification(@RequestBody Employee_Qualification qualification) {
        qualificationService.add_qualification(qualification);
    }

    @PutMapping("/updatequalification/{id}")
    public void update_qualification(@PathVariable long id ,@RequestBody Employee_Qualification updatequalification) {
        qualificationService.update_qualification(id,updatequalification);
    }
    @DeleteMapping("/deletequalification/{id}")
    public void deletequalification(@PathVariable Long id){
        qualificationService.delete_qualification(id);
    }

   /* //Admin
    @GetMapping("/getalladmin")
    public List<Admin> getalladmin() {
        return adminService.get_all_Admin();
    }

    @GetMapping("/findadmin/{id}")
    public Optional<Admin> findadmin(@PathVariable long id){
        return adminService.get_Admin_by_id(id);
    }
    @PostMapping("/addadmin")
    public void Addadmin(@RequestBody Admin admin) {
        adminService.add_Admin(admin);
    }

    @PutMapping("/updateadmin/{id}")
    public void update_admin(@PathVariable long id ,@RequestBody Admin updateadmin) {
        adminService.update_Admin(id,updateadmin);
    }
    @DeleteMapping("/deleteadmin/{id}")
    public void deleteadmin(@PathVariable Long id){
        adminService.delete_Admin(id);
    }*/

}

