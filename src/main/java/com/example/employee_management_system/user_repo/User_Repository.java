package com.example.employee_management_system.user_repo;

import com.example.employee_management_system.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface User_Repository extends JpaRepository<User,Long> {
    User findByUsername(String username);
}
