package com.example.employee_management_system.Repository_Department;

import com.example.employee_management_system.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Department_Repository extends JpaRepository<Department,Long> {
}
