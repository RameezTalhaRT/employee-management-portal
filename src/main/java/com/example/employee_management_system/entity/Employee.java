package com.example.employee_management_system.entity;


import jakarta.persistence.*;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String empimage;
    private String empname;
    private String dob;
    private String emailaddress;
    private String homeaddress;
    private String martialstatus;
    private long salary;
    private String phoneno;
    private String country;
    private String province;
    private String region;

    @ManyToOne
    @JoinColumn(name = "qualification_id")
    private Employee_Qualification qualification;

    /*@ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;*/

     @ManyToOne
    @JoinColumn(name = "skill_id")
    private Skill skill;

    @ManyToOne
    @JoinColumn(name = "experience_id")
    private Emp_Background_Exp experience;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmpname() {
        return empname;
    }

    public void setEmpname(String empname) {
        this.empname = empname;
    }

    public String getEmpimage() {
        return empimage;
    }

    public void setEmpimage(String empimage) {
        this.empimage = empimage;
    }

    public String getMartialstatus() {
        return martialstatus;
    }

    public void setMartialstatus(String martialstatus) {
        this.martialstatus = martialstatus;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getHomeaddress() {
        return homeaddress;
    }

    public void setHomeaddress(String homeaddress) {
        this.homeaddress = homeaddress;
    }

    public long getSalary() {
        return salary;
    }

    public int age;

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Emp_Background_Exp getExperience() {
        return experience;
    }

    public void setExperience(Emp_Background_Exp experience) {
        this.experience = experience;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Employee_Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Employee_Qualification qualification) {
        this.qualification = qualification;
    }
}


