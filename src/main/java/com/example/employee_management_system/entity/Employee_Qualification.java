package com.example.employee_management_system.entity;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "qualification")
public class Employee_Qualification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String school_name;
    private String school_join_date;
    private String school_end_date;
    private String school_field;
    private String school_grade;

    @OneToMany(mappedBy ="qualification" )
    private Set<Employee> employees;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getSchool_join_date() {
        return school_join_date;
    }

    public void setSchool_join_date(String school_join_date) {
        this.school_join_date = school_join_date;
    }

    public String getSchool_end_date() {
        return school_end_date;
    }

    public void setSchool_end_date(String school_end_date) {
        this.school_end_date = school_end_date;
    }

    public String getSchool_field() {
        return school_field;
    }

    public void setSchool_field(String school_field) {
        this.school_field = school_field;
    }

    public String getSchool_grade() {
        return school_grade;
    }

    public void setSchool_grade(String school_grade) {
        this.school_grade = school_grade;
    }

    /*public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }*/
}

