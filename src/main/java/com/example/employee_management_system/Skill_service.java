package com.example.employee_management_system;

import com.example.employee_management_system.Repository_Skill.Skill_Repository;
import com.example.employee_management_system.entity.Skill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Skill_service {
    @Autowired
    private Skill_Repository skillRepository;
    public void add_skill(Skill skill){
        skillRepository.save(skill);
    }
    public void delete_skill(Long id){
        skillRepository.deleteById(id);
    }
    public Optional<Skill> get_skill_by_id(Long id){
        return Optional.ofNullable(skillRepository.findById(id).orElse(null));
    }
    public List<Skill> get_all_skill(){
        return skillRepository.findAll();
    }

    public void update_skll(Long id, Skill updateskill){
        if (skillRepository.existsById(id)){
            updateskill.setId(id);
            skillRepository.save(updateskill);
        }
    }
}

