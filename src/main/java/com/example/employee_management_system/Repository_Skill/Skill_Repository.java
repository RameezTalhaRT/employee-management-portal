package com.example.employee_management_system.Repository_Skill;

import com.example.employee_management_system.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Skill_Repository extends JpaRepository<Skill,Long> {
}
