package com.example.employee_management_system.Repository_Qualification;

import com.example.employee_management_system.entity.Employee_Qualification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Qualification_Repository extends JpaRepository<Employee_Qualification,Long> {
}
