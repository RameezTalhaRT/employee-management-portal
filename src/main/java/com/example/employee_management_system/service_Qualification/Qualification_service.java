package com.example.employee_management_system.service_Qualification;


import com.example.employee_management_system.Repository_Qualification.Qualification_Repository;
import com.example.employee_management_system.entity.Employee_Qualification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Qualification_service {
    @Autowired

    private Qualification_Repository qualificationRepository;
    public void add_qualification(Employee_Qualification qualification){
        qualificationRepository.save(qualification);
    }
    public void delete_qualification(Long id){
        qualificationRepository.deleteById(id);
    }
    public Optional<Employee_Qualification> get_qualification_by_id(Long id){
        return Optional.ofNullable(qualificationRepository.findById(id).orElse(null));
    }
    public List<Employee_Qualification> get_all_qualification(){
        return qualificationRepository.findAll();
    }

    public void update_qualification(Long id, Employee_Qualification updatequalification){
        if (qualificationRepository.existsById(id)){
            updatequalification.setId(id);
            qualificationRepository.save(updatequalification);
        }
    }
}
